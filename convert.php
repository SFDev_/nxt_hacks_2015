<?php 
require('includes/header.php'); 
require('includes/convert_api_info.php');
require('includes/convert.inc.php');
?>
    
    
        <section id="convert_interface">
            <h1>BTC > NXT</h1>
            
            <ul>
                <li>Rate : <span><?php echo $ss_rate; ?></span> LTC = <span>1</span> BTC</li>
                <li>Max Amnt : <span><?php echo $ss_limit;; ?></span> BTC</li>
                <li>Min Amnt : <span><?php echo number_format($ss_min, 8, '.', ''); ?></span> BTC</li>
            </ul>
            <form id="getnxtaddress" method="post" action="<?php echo $_SERVER[ 'PHP_SELF' ]; ?>">
                <input name="address" type="text" placeholder="NXT Address" id="address" />
                <input name="pubkey" type="text" placeholder="NXT Public Key" id="pubkey" />
                <input type="submit" name="Convert" id="convert" value="Convert">
            </form>
            <div id="result">
                <?php 
                    echo $message;
                    echo $qr;
                ?>
                
            </div>
        </section>

<?php 
//include footer template
require('includes/footer.php'); 
?>
