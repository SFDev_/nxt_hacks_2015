<?php
require('includes/header.php');
?>
<section id="trade_page" >
    <h1>Currencies</h1>
    <div id="tab-container">
    <ul class="list" >
        <?php 
        require('includes/functions.inc.php');
        $req1 = 'getAllCurrencies';
        $Currencies = SendReq($req1);
        $res1 = json_decode($Currencies,true);
        $currencyCode = $res1['currencies'][0]['code'];
        $currencyId = $res1['currencies'][0]['currency'];

        $x = 0;

        while($currencyCode != '') 
        {
            echo '<li><a href=#'. $currencyCode.'>' .  $currencyCode . '</a></li>';
            $x = $x + 1;
            $currencyCode = $res1['currencies'][$x]['code'];
            $currencyId = $res1['currencies'][$x]['currency'];
        } 
        ?>
    </ul>
    
<?php

        $currencyIdb = $res1['currencies'][0]['currency'];
        $req2 = 'getCurrency&currency=' . $currencyIdb;

        $response = SendReq($req2);
        $res2 = json_decode($response,true);
        $currencyNameB = $res2['name'];
        $currencyCodeB = $res2['code'];
        $currencyCurrentSupplyB = $res2['currentSupply'];
        $currencyAccount = $res2['accountRS'];
        $currencyDesc = $res2['description'];
        $y = 0;
        
        while($currencyCodeB != '')
        {
        echo'<div class="asset_description" id="' .$currencyCodeB. '">';
        echo '<h3>' . $currencyNameB . '</h3>';
        echo '<p class="infos">Code : <span>' . $currencyCodeB . '</span></p>';
        echo '<p class="infos">Current Supply : <span>' . $currencyCurrentSupplyB . '</span></p>';
        echo '<p class="infos">Account : <span>' . $currencyAccount . '</span></p>';
        echo '<p class="infos">Description : <span>' . $currencyDesc . '</span></p>';
        echo '<br><br><br><br>';
        $y = $y + 1;
        $currencyIdb = $res1['currencies'][$y]['currency'];
        $req2 = 'getCurrency&currency=' . $currencyIdb;
        $response = SendReq($req2);
        $res2 = json_decode($response,true);
        $currencyNameB = $res2['name'];
        $currencyCodeB = $res2['code'];
        $currencyCurrentSupplyB = $res2['currentSupply'];
        $currencyAccount = $res2['accountRS'];
        $currencyDesc = $res2['description'];
        
        echo '</div>';
        }
    ?> 
        
</div>
    <br><br>
    
</section>
<?php
require('includes/footer.php');
?>