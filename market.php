<?php
require('includes/header.php');
?>
<section id="market_page">
    <h1>Goods</h1>
        <div id="tab-container">

    <ul class="list">

        
        <?php 
        require('includes/functions.inc.php');
        $req1 = 'getDGSGoods';
        $Goods = SendReq($req1);
        $res1 = json_decode($Goods,true);
        $GoodTag = $res1['goods'][0]['name'];

        $x = 0;

        while($GoodTag != '') 
        {
            $name = substr($GoodTag, 0, 40);
            echo '<li><a href=#'. $name.'>' .  $name . '</a></li>';
            $x = $x + 1;
            $GoodTag = $res1['goods'][$x]['tags'];
        } 
        ?>
        
    </ul>
    <?php

        $GoodId = $res1['goods'][0]['goods'];
        $req2 = 'getDGSGood&goods=' . $GoodId;

        $response = SendReq($req2);
        $res2 = json_decode($response,true);
        $goodName = $res2['name'];
        $goodSeller= $res2['sellerRS'];
        $goodQuantity = $res2['quantity'];
        $goodTags = $res2['tags'];
        $goodDesc = $res2['description'];
        $y = 0;
        
        while($goodName != '')
        {
        echo'<div class="asset_description" id="' .$goodName. '">';
        echo '<h3>' . $goodName . '</h3>';
        echo '<p class="infos">Seller : <span>' . $goodSeller . '</span></p>';
        echo '<p class="infos">Quantity : <span>' . $goodQuantity . '</span></p>';
        echo '<p class="infos">Tags : <span>' . $goodTags . '</span></p>';
        echo '<p class="infos">Description : <span>' . $goodDesc . '</span></p>';
        echo '<br><br><br><br>';
        $y = $y + 1;
        $GoodId = $res1['goods'][$y]['goods'];
        $req2 = 'getDGSGood&goods=' . $GoodId;
        $response = SendReq($req2);
        $res2 = json_decode($response,true);
        $goodName = $res2['name'];
        $goodSeller= $res2['sellerRS'];
        $goodQuantity = $res2['quantity'];
        $goodTags = $res2['tags'];
        $goodDesc = $res2['description'];
        
        echo '</div>';
        }
    ?> 
    
</div>
    <br><br>
    
</section>
<?php
require('includes/footer.php');
?>