<?php
require('includes/header.php');
?>

<section id="trade_page">

    <h1>Assets</h1>
    <div id="tab-container">

        
    <ul class="list">
        <?php 
            require('includes/all_assets.inc.php');

        $x = 0;
        
        while($assetName != '')
        {
            echo '<li><a href=#'.$assetName.'>' . $assetName . '</a></li>';
            $x = $x + 1;
            $assetName = $res['assets'][$x]['name'];
            $assetId = $res['assets'][$x]['asset'];
        }
                ?>
        
    </ul>
        
        
    <?php

        $assetIdb = $res['assets'][0]['asset'];
        $req1 = 'getAsset&asset=' . $assetIdb;

        $response = SendReq($req1);
        $res1 = json_decode($response,true);
        $assetNameBox = $res1['name'];
        $assetAccount = $res1['accountRS'];
        $assetQuantityBox = $res1['quantityQNT'];
        $assetDescBox = $res1['description'];
        $y = 0;
        
        while($assetNameBox != '')
        {
        echo'<div class="asset_description" id="' .$assetNameBox. '">';
        echo '<h3>' . $assetNameBox . '</h3>';
        echo '<p class="infos">Account : <span>' . $assetAccount . '</span></p>';
        echo '<p class="infos">Quantity : <span>' . $assetQuantityBox . '</span></p>';
        echo '<p class="infos">Description : <span>' . $assetDescBox . '</span></p>';
        echo '<br>';
        $y = $y + 1;
        $assetIdb = $res['assets'][$y]['asset'];
        $req1 = 'getAsset&asset=' . $assetIdb;
        $response = SendReq($req1);
        $res1 = json_decode($response,true);
        $assetNameBox = $res1['name'];
        $assetAccount = $res1['accountRS'];
        $assetQuantityBox = $res1['quantityQNT'];
        $assetDescBox = $res1['description'];
        echo '</div>';
        }
    ?>  
    </div>
    <br><br>
    
</section>
<?php
require('includes/footer.php');
?>