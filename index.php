<?php
require('includes/header.php');
?>
<script>
    $(document).ready(function(){
        swal("Welcome!", "This Website allow you to track all available Assets, Currencies and Goods available in the NXT Decentralized exchange and Marketplace.\n\nFuture Updates will allow users to Execute their orders on the decentralized exchange through our website  \n\nSFDev\nNXT-EV3W-4R77-L5TQ-5DM7Y")
    });
</script>
    <main>
        <h1>NXT Infos</h1>
        
        <h2>Assets</h2>
        <table>
            <tr>
                <th>Asset Name</th>
                <th>QNT</th>
                <th>Description</th>
            </tr>
            
        <?php
        require('includes/all_assets.inc.php');
        $x = 0;
        for ($i=1; $i<=15; $i++){ 
        while($assetName != '') 
        {
            echo "<tr>";
            echo "<td>" . $assetName . "</td>";  
            echo "<td>" . $assetQuantity . "</td>"; 
            echo "<td>" . $assetDesc . "</td>";
            $x = $x + 1;
            $assetName = $res['assets'][$x]['name'];
            $assetQuantity = $res['assets'][$x]['quantityQNT'];
            $assetDesc = $res['assets'][$x]['description'];
            echo "</tr>";
        }
        }
        ?>
            
        </table>
        
<!--        Currencies-->
        <h2>Currencies</h2>
        <table>
            <tr>
                <th>Currency Code</th>
                <th>Current Supply</th>
                <th>Max Supply</th>
                <th>Description</th>
            </tr>
<?php
        $req1 = 'getAllCurrencies';
        $Currencies = SendReq($req1);
        $res1 = json_decode($Currencies,true);
        $currencyCode = $res1['currencies'][0]['code'];
        $currencyDesc = $res1['currencies'][0]['description'];
        $currencyCurrentSupply = $res1['currencies'][0]['currentSupply'];
        $currencyMaxSupply = $res1['currencies'][0]['maxSupply'];

        $y = 0;
        for ($i=1; $i<=15; $i++){ 
        while($currencyCode != '') 
        {
            echo "<tr>";
            echo "<td>" . $currencyCode . "</td>";  
            echo "<td>" . $currencyCurrentSupply . "</td>"; 
            echo "<td>" . $currencyMaxSupply . "</td>";
            echo "<td>" . $currencyDesc . "</td>";
            echo "</tr>";
            $y = $y + 1;
            $currencyCode = $res1['currencies'][$y]['code'];
            $currencyDesc = $res1['currencies'][$y]['description'];
            $currencyCurrentSupply = $res1['currencies'][$y]['currentSupply'];
            $currencyMaxSupply = $res1['currencies'][$y]['maxSupply'];
        } 
        }

        ?>
        </table>
        
    <img alt="NXTLogo" src="img/nxt-logo2.png" id="LogoFoot"/> 
    <h2>Nxt Hacks 2015 - SFDev</h2>
    </main>
    <?php
    require('includes/footer.php');
    ?>