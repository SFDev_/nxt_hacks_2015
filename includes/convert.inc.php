<?php
if(isset($_POST['Convert'])){
    $nxtadd = $_POST['address'];
    $pubkey = $_POST['pubkey'];
    $pair = 'btc_Nxt';
    $arr_req = array("withdrawal" => $nxtadd,"pair" => $pair,"rsAddress" => $pubkey);
    $req = json_encode($arr_req);
    
    $ch = curl_init('https://shapeshift.io/shift');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',   
    'Content-Length: ' . strlen($req))                 
               );                           
    $result = curl_exec($ch);
    $res = json_decode($result,true);
    $bitadd = $res['deposit'];
    
    $message = "<p id='payment_not'>Please Send Your Payment to : <span>" . $bitadd . "</span></p>";
    $qr = '<img id="qrcode" src="https://chart.googleapis.com/chart?chld=H|2&chs=225x225&cht=qr&chl=' . $bitadd . '"/><br><br>';
}